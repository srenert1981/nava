<?php

const BASEDIR = "/nava";
function getRoot()
{
    return dirname(__DIR__) . BASEDIR;
}

function process()
{
    try {
        $results = [];
        $schemas = Schema::getAll(getRoot());
        foreach ($schemas as $schema) {
            $measures = new Measures($schema);
            if ($measures->load()) {
                foreach ($measures->getItems() as $measure) {
                    $post = new PostData("https://2swdepm0wa.execute-api.us-east-1.amazonaws.com/prod/NavaInterview/measures", $measure);
                    $result = $post->send();
                    array_push($results, $result);
                }
            }
        }
    } catch (Exception $exception) {
        return json_encode([
            'success' => false,
            'error' => $exception->getMessage()
        ]);
    }
    return json_encode([
        'success' => true,
        'data' => $results
    ]);
}