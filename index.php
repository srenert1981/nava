<?php
require_once("controller.php");
require_once("Classes/Schema/Schema.php");
require_once("Classes/Schema/SchemaField.php");
require_once("Classes/DataField/DataField.php");
require_once("Classes/DataField/BooleanDataField.php");
require_once("Classes/DataField/TextDataField.php");
require_once("Classes/DataField/IntegerDataField.php");
require_once("Classes/Measures/Measures.php");
require_once("Classes/Measures/Measure.php");
require_once("Classes/PostData.php");

try {
    echo process();
} catch (Exception $e) {
    echo $e->getMessage();
}
die();