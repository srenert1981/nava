# README #

Nava Software Engineer Takehome Exercise.

### What is the purpose of this project? ###

* Iterate through potential measures and post to API
* Version 1.0

### How do I get set up? ###

* Clone repo from https://srenert1981@bitbucket.org/srenert1981/nava.git (or extract the .zip attached)
* Configure nginx/apache host 
* Go to root of project to begin processing

### Who do I talk to? ###

* Simon Renert (srenert@gmail.com)