<?php

class PostData
{
    private $measure;
    private $host;

    public function __construct($host, $measure) {
        $this->measure = $measure;
        $this->host = $host;
    }

    /**
     * @param int $timeout
     */
    public function send($timeout = 10) {
        $payload = $this->measure->getJsonPayload();

        $curl = curl_init();
        $opts[CURLOPT_URL] = $this->host;
        $opts[CURLOPT_RETURNTRANSFER] = true;
        $opts[CURLOPT_SSL_VERIFYPEER] = false;
        $opts[CURLOPT_CONNECTTIMEOUT] = $timeout;
        $opts[CURLOPT_POSTFIELDS] = $payload;
        curl_setopt_array($curl, $opts);

        $curlResponse = curl_exec($curl);

        if ($curlResponse === false) {
            return [
                'status' => 'error',
                'error' => curl_errno($curl),
                'message' => curl_error($curl)
            ];
        } else {
            return [
                'status' => 'success',
                'data' => $curlResponse
            ];
        }
    }
}
