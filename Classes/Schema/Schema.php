<?php

class Schema
{
    const DIR = "schemas";
    private $fields = [];
    private $name;
    private $rootPath;

    public function __construct($rootPath, $fileName) {
        $this->name = $fileName;
        $this->rootPath = $rootPath;
    }

    public function getName() {
        return $this->name;
    }
    public function getRootPath() {
        return $this->rootPath;
    }
    public function getFields() {
        return $this->fields;
    }

    /**
     * @param $root
     * @return array
     * @throws \Exception
     */
    public static function getAll($root)
    {
        $schemas = [];
        if (is_dir($root . "/" . self::DIR)) {
            $files = array_filter(scandir($root . "/" . self::DIR), function ($filename) {
                if ($filename == "." || $filename === "..") return false;
                return true;
            });
            foreach ($files as $file) {
                $schema = new Schema($root, $file);
                $schema->parse();
                array_push($schemas, $schema);
            }
        }
        return $schemas;
    }

    /**
     * @throws \Exception
     */
    private function parse() {
        $path = $this->rootPath . "/" . self::DIR . "/" . $this->name;
        if (file_exists($path)) {
            $contents = file_get_contents($path);
            if (empty($contents)) throw new Exception("$path missing schema");
            $lines = explode( PHP_EOL, $contents);
            foreach ($lines as $line) {
                if ($line) {
                    $cols = explode(",", $line);
                    $field = new SchemaField($cols[0], $cols[1], $cols[2]);
                    array_push($this->fields, $field);
                }
            }
        }
    }
}
