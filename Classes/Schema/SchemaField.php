<?php

class SchemaField
{
    private $fieldName;
    private $width;
    private $sqlType;

    public function __construct($fieldName, $width, $sqlType) {
        $this->fieldName = $fieldName;
        $this->width = $width;
        $this->sqlType = $sqlType;
    }

    public function getName() {
        return $this->fieldName;
    }
    public function getWidth() {
        return $this->width;
    }
    public function getSqlType() {
        return $this->sqlType;
    }
}
