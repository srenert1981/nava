<?php

class Measures
{
    private $schema;
    private $measures = [];

    public function __construct($schema) {
        $this->schema = $schema;
    }

    public function getItems()
    {
        return $this->measures;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function load()
    {
        $added = 0;
        $dataPath = $this->schema->getRootPath() . "/data/" . str_replace(".csv", ".txt", $this->schema->getName());

        if(file_exists($dataPath)) {
            $contents = file_get_contents($dataPath);
            if (!$contents) throw new Exception("Data file for " . $this->schema->getName() . " is empty");
            $lines = explode(PHP_EOL, $contents);
            foreach($lines as $line) {
                if ($line) {
                    $starIdx = 0;
                    $measure = new Measure();
                    foreach ($this->schema->getFields() as $schemaField) {
                        $item = substr($line, $starIdx, $schemaField->getWidth());
                        $starIdx+= $schemaField->getWidth();
                        $dataField = DataField::createDataField($schemaField->getSqlType(), $schemaField->getName(), $item);
                        $measure->addField($dataField);
                    }
                    array_push($this->measures, $measure);
                    $added++;
                }
            }
        }
        return $added > 0;
    }
}
