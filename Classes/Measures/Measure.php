<?php

class Measure
{
    private $fields = [];

    public function addField($field) {
        array_push($this->fields, $field);
    }
    public function getFields() {
        return $this->fields;
    }

    /**
     * @return false|string
     */
    public function getJsonPayload() {
        $payload = [];
        foreach($this->fields as $name => $dataField) {
            $payload[$dataField->getName()] = $dataField->getValue();
        }
        return json_encode($payload);
    }
}
