<?php

class BooleanDataField extends DataField
{
    const DATA_TYPE = "BOOLEAN";
    public function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }
    public function getValue() {
        return ($this->value ? true : false );
    }
}