<?php

class IntegerDataField extends DataField
{
    const DATA_TYPE = "INTEGER";
    public function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }
    public function getValue() {
        return (int)$this->value;
    }
}