<?php

class TextDataField extends DataField
{
    const DATA_TYPE = "TEXT";
    public function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }
    public function getValue() {
        return $this->value;
    }
}