<?php

class DataField
{
    protected $value;
    protected $name;

    /**
     * @param $type
     * @param $name
     * @param $value
     * @return BooleanDataField|IntegerDataField|TextDataField
     */
    public static function createDataField($type, $name, $value) {
        switch($type) {
            case TextDataField::DATA_TYPE:
                $dt = new TextDataField($name, $value);
                break;
            case IntegerDataField::DATA_TYPE:
                $dt = new IntegerDataField($name, $value);
                break;
            case BooleanDataField::DATA_TYPE:
                $dt = new BooleanDataField($name, $value);
                break;
        }
        return $dt;
    }

    public function getName() {
        return $this->name;
    }

    public function getPair() {
        return [
            $this->name => $this->value
        ];
    }
}